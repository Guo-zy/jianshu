import axios from 'axios';
import * as constants from './actionType';
const network = axios.create({
  baseURL: 'http://localhost:3000',
  timeout: 5000,
});

const changeLogin = () => ({
  type: constants.CHANGE_LOGIN,
  value: true,
});

export const logout = () => ({
  type: constants.CHANGE_LOGIN,
  value: false,
});

export const login = (account, password) => {
  return dispatch => {
    network({
      url: '/api/login.json?account=' + account + '&password=' + password,
      method: 'get',
    })
      .then(res => {
        const result = res.data.data;
        if (result) {
          dispatch(changeLogin());
        } else {
          alert('登录失败');
        }
      })
      .catch(e => {
        console.log(e);
      });
  };
};
