import { fromJS } from 'immutable';
import * as constants from '../action/actionType';

const defaultState = fromJS({
  login: false,
});

const reducer = (state = defaultState, action) => {
  switch (action.type) {
    case constants.CHANGE_LOGIN:
      return state.set('login', action.value);
    default:
      return state;
  }
};

export default reducer;
