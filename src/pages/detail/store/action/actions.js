import * as constants from './actionType';
import axios from 'axios';

const network = axios.create({
  baseURL: 'http://localhost:3000',
  timeout: 5000,
});

const getDetailInfoAction = data => ({
  type: constants.CHANGE_DETAIL,
  data,
});

export const getDetailInfo = id => {
  return dispatch => {
    network({
      url: '/api/detail.json',
      method: 'get',
    })
      .then(res => {
        dispatch(getDetailInfoAction(res.data.data));
      })
      .catch(e => {
        console.log(e);
      });
  };
};
