import * as constans from '../action/actionType';
import { fromJS } from 'immutable';

const defaluState = fromJS({
  title: '',
  content: '',
});

const reducer = (state = defaluState, action) => {
  switch (action.type) {
    case constans.CHANGE_DETAIL:
      return state.merge({
        title: action.data.title,
        content: action.data.content,
      });
    default:
      return state;
  }
};

export default reducer;
