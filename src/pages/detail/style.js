import styled from 'styled-components';
import detailImageUrl from '../../statics/detailPic.png';

export const DetailWrapper = styled.div`
  overflow: hidden;
  width: 620px;
  margin: 0 auto;
  padding-bottom: 100px;
`;

export const Header = styled.div`
  margin: 50px 0 20px 0;
  font-size: 34px;
  line-height: 44px;
  color: #333;
  font-weight: bold;
`;

export const Content = styled.div`
  color: #2f2f2f;
`;
export const DetailImage = styled.img.attrs({
  src: `${detailImageUrl}`,
  alt: '插图',
})`
  widht: 666px;
  height: 425px;
  margin-bottom: 75px;
`;
export const ContentItem = styled.div`
  p {
    padding: 20px;
    background: #fafafa;
    border-left: 6px solid #e6e6e6;
    font-size: 16px;
    font-weight: normal;
    line-height: 30px;
    margin-bottom: 20px;
  }
`;
