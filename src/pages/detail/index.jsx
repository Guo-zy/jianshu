import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  DetailWrapper,
  Header,
  Content,
  DetailImage,
  ContentItem,
} from './style';
import { actionCreator } from './store';
import { withRouter } from 'react-router-dom';

class Detail extends Component {
  render() {
    const { title, content } = this.props;
    return (
      <DetailWrapper>
        <Header>{title}</Header>
        <Content>
          <DetailImage />
          <ContentItem dangerouslySetInnerHTML={{ __html: content }} />
        </Content>
      </DetailWrapper>
    );
  }

  componentDidMount() {
    const { getDetailInfo, match } = this.props;
    getDetailInfo(match.params.id);
  }
}

const mapStateToProps = state => ({
  title: state.getIn(['detail', 'title']),
  content: state.getIn(['detail', 'content']),
});

const mapDispatchToProps = dispatch => ({
  getDetailInfo(id) {
    dispatch(actionCreator.getDetailInfo(id));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Detail));
