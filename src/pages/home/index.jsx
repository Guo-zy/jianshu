import React, { PureComponent } from 'react';
import List from './components/list';
import Recommend from './components/recommend';
import { actionCreator } from './store';
import { connect } from 'react-redux';
import { HomeWrapper, HomeLeft, HomeRight, BackTop } from './style';
class Home extends PureComponent {
  render() {
    const { showScroll } = this.props;
    return (
      <HomeWrapper>
        <HomeLeft>
          <List />
        </HomeLeft>
        <HomeRight>
          <Recommend />
        </HomeRight>
        {showScroll ? (
          <BackTop onClick={() => this.handleScrollTop()}>回到顶部</BackTop>
        ) : null}
      </HomeWrapper>
    );
  }
  componentDidMount() {
    const { getHomeList } = this.props;
    getHomeList();
    this.bindEvents();
  }
  componentWillUnmount() {
    const { changeScrollTopShow } = this.props;
    window.removeEventListener('scroll', changeScrollTopShow);
  }

  handleScrollTop() {
    //返回顶部
    window.scrollTo(0, 0);
  }
  bindEvents() {
    const { changeScrollTopShow } = this.props;
    window.addEventListener('scroll', changeScrollTopShow);
  }
}

const mapStateToProps = state => ({
  showScroll: state.getIn(['home', 'showScroll']),
});

const mapDispatchToProps = dispatch => ({
  getHomeList() {
    dispatch(actionCreator.getHomeList());
  },
  changeScrollTopShow() {
    if (document.documentElement.scrollTop >= 300) {
      dispatch(actionCreator.toggleTopShow(true));
    } else {
      dispatch(actionCreator.toggleTopShow(false));
    }
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
