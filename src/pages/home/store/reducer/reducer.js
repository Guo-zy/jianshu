import { fromJS } from 'immutable';
import * as constants from '../action/actionType';

const defaultState = fromJS({
  infoList: [],
  recommendList: [],
  showScroll: false,
});

const reducer = (state = defaultState, action) => {
  switch (action.type) {
    case constants.INIT_HOME_LIST_INFO:
      return state.merge({
        infoList: action.infoList,
        recommendList: action.recommendList,
      });
    case constants.ADD_MORE_LIST_INFO:
      return state.set(
        'infoList',
        state.get('infoList').concat(action.infoList)
      );
    case constants.TOGGLE_SCROLL_TOP:
      return state.set('showScroll', action.isShow);
    default:
      return state;
  }
};

export default reducer;
