import * as constants from './actionType';
import axios from 'axios';
import { fromJS } from 'immutable';
const network = axios.create({
  baseURL: 'http://localhost:3000',
  timeout: 5000,
});

const addHomeList = (infoList, recommendList) => ({
  type: constants.INIT_HOME_LIST_INFO,
  infoList: fromJS(infoList),
  recommendList: fromJS(recommendList),
});

const addMoreList = infoList => ({
  type: constants.ADD_MORE_LIST_INFO,
  infoList: fromJS(infoList),
});

export const toggleTopShow = isShow => ({
  type: constants.TOGGLE_SCROLL_TOP,
  isShow,
});

export const getHomeList = () => {
  return dispatch => {
    network({
      url: 'api/homeList.json',
      method: 'get',
    })
      .then(res => {
        dispatch(addHomeList(res.data.infoList, res.data.recommendList));
      })
      .catch(e => console.log(e));
  };
};

export const getMoreList = () => {
  return dispatch => {
    network({
      url: 'api/homeList.json',
      method: 'get',
    })
      .then(res => {
        dispatch(addMoreList(res.data.infoList));
      })
      .catch(e => console.log(e));
  };
};
