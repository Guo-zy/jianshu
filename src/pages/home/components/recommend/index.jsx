import React, { PureComponent } from 'react';
import { connect } from 'react-redux';
import { RecommendWrapper, RecommendItem } from '../../style';

class Recommend extends PureComponent {
  render() {
    return <RecommendWrapper>{this.getList()}</RecommendWrapper>;
  }

  getList = () => {
    const { recommendList } = this.props;
    const recommendNewList = recommendList.toJS();
    if (recommendNewList.length !== 0) {
      return recommendNewList.map(item => {
        return (
          <RecommendItem imgUrl={item.imgUrl} key={item.id}></RecommendItem>
        );
      });
    }
  };
}

const mapStateToProps = state => ({
  recommendList: state.getIn(['home', 'recommendList']),
});

const mapDispatchToProps = dispatch => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Recommend);
