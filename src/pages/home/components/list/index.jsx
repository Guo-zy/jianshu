import React, { PureComponent, Fragment } from 'react';
import { connect } from 'react-redux';
import { ListItem, ListInfo, ListImg, LoadMore } from '../../style';
import { actionCreator } from '../../store';
import { Link } from 'react-router-dom';

class List extends PureComponent {
  render() {
    return (
      <Fragment>
        {this.handleGetList()}
        <LoadMore onClick={() => this.handleGetMoreList()}>更多文字</LoadMore>
      </Fragment>
    );
  }

  handleGetMoreList = () => {
    const { getMoreList } = this.props;
    getMoreList();
  };

  handleGetList = () => {
    const { infoList } = this.props;
    const infoNewList = infoList.toJS();
    if (infoNewList.length !== 0) {
      return infoNewList.map((item, index) => {
        return (
          <Link key={index} to={'/detail/' + index}>
            <ListItem key={index}>
              <ListImg className="image" imgUrl={item.imgUrl} />
              <ListInfo>
                <h3 className="title">{item.title}</h3>
                <p className="content">{item.content}</p>
              </ListInfo>
            </ListItem>
          </Link>
        );
      });
    }
  };
}

const mapStateToProps = state => ({
  infoList: state.getIn(['home', 'infoList']),
});

const mapDispatchToProps = dispatch => ({
  getMoreList() {
    dispatch(actionCreator.getMoreList());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(List);
