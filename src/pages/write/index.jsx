import React, { PureComponent } from 'react';
import { connect } from 'react-redux';

import { Redirect } from 'react-router-dom';

class WriteArticle extends PureComponent {
  render() {
    const { loginStatus } = this.props;
    if (!loginStatus) {
      return <Redirect to="/" />;
    } else {
      return <div>写文章页面</div>;
    }
  }
}

const mapStateToProps = state => ({
  loginStatus: state.getIn(['login', 'login']),
});

export default connect(mapStateToProps, null)(WriteArticle);
