import React, { Component } from 'react';
import { connect } from 'react-redux';
import { actionCreators } from './store/index';
import { actionCreator as loginActionCreators } from '../../pages/login/store';
// import { CSSTransition } from 'react-transition-group';
import { Link } from 'react-router-dom';
import {
  HeaderWrapper,
  Logo,
  Nav,
  NavItem,
  NavSerarch,
  Addition,
  Button,
  SearchWrapper,
  SearchInfo,
  SearchInfoTitle,
  SearchInfoSwitch,
  SearchInfoItemWrapper,
  SearchInfoItem,
} from './style';

class Header extends Component {
  render() {
    const { focused, handleInputFocus, handleInputBlur, list, login, logout } =
      this.props;
    return (
      <HeaderWrapper>
        <Link to="/">
          <Logo />
        </Link>
        <Nav>
          <NavItem className="left active">首页</NavItem>
          <NavItem className="left">下载App</NavItem>
          {login ? (
            <NavItem className="right" onClick={() => logout()}>
              退出
            </NavItem>
          ) : (
            <Link to="/login">
              <NavItem className="right">登录</NavItem>
            </Link>
          )}
          <NavItem className="right">Aa</NavItem>
          <SearchWrapper>
            <NavSerarch
              className={focused ? 'focused' : ''}
              onFocus={() => handleInputFocus(list)}
              onBlur={() => handleInputBlur()}
            />
            {this.getListArea()}
          </SearchWrapper>
        </Nav>
        <Addition>
          <Link to="write">
            <Button className="writting">写文章</Button>
          </Link>
          <Button className="reg">注册</Button>
        </Addition>
      </HeaderWrapper>
    );
  }
  getListArea = () => {
    const {
      focused,
      list,
      currentPage,
      totalPage,
      handleMouseEnter,
      handleMouseLeave,
      mouseIn,
      handleChangePage,
    } = this.props;
    const jsList = list.toJS();
    const pageList = [];
    if (jsList.length) {
      for (let i = (currentPage - 1) * 10; i < currentPage * 10; i++) {
        if (!jsList[i]) break;
        pageList.push(
          <SearchInfoItem key={jsList[i]}>{jsList[i]}</SearchInfoItem>
        );
      }
    }
    if (focused || mouseIn) {
      return (
        <SearchInfo
          onMouseEnter={() => handleMouseEnter()}
          onMouseLeave={() => handleMouseLeave()}
        >
          <SearchInfoTitle>
            热门搜索{' '}
            <SearchInfoSwitch
              onClick={() => handleChangePage(currentPage, totalPage)}
            >
              换一批
            </SearchInfoSwitch>
          </SearchInfoTitle>
          <SearchInfoItemWrapper>{pageList}</SearchInfoItemWrapper>
        </SearchInfo>
      );
    } else {
      return null;
    }
  };
}

const mapStateToProps = state => ({
  // focused: state.get('header').get('focused'),
  focused: state.getIn(['header', 'focused']),
  list: state.getIn(['header', 'list']),
  currentPage: state.getIn(['header', 'currentPage']),
  totalPage: state.getIn(['header', 'totalPage']),
  mouseIn: state.getIn(['header', 'mouseIn']),
  login: state.getIn(['login', 'login']),
});

const mapDispatchToProps = dispatch => ({
  handleInputFocus(list) {
    list.size === 0 && dispatch(actionCreators.getList());
    dispatch(actionCreators.searchFocus());
  },
  handleInputBlur() {
    dispatch(actionCreators.searchBlur());
  },
  handleMouseEnter() {
    dispatch(actionCreators.mouseEnter());
  },
  handleMouseLeave() {
    dispatch(actionCreators.mouseLeave());
  },
  handleChangePage(currentPage, totalPage) {
    currentPage = currentPage >= totalPage ? 1 : currentPage + 1;
    dispatch(actionCreators.changePage(currentPage));
  },
  logout() {
    dispatch(loginActionCreators.logout());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
