import * as constans from '../action/actionType';
import axios from 'axios';
import { fromJS } from 'immutable';

const network = axios.create({
  baseURL: 'http://localhost:3000',
  timeout: 5000,
});
const changeList = data => ({
  type: constans.CHANGE_LIST,
  data: fromJS(data),
  totalPage: Math.ceil(data.length / 10),
});

export const searchFocus = () => ({
  type: constans.SERACH_FOCUSED,
});
export const searchBlur = () => ({
  type: constans.SEARCH_BLUR,
});
export const mouseEnter = () => ({
  type: constans.MOUSE_ENTER,
});
export const mouseLeave = () => ({
  type: constans.MOUSE_LEAVE,
});
export const changePage = currentPage => ({
  type: constans.CHANGE_PAGE,
  currentPage,
});

export const getList = () => {
  return dispatch => {
    network({
      url: '/api/headerList.json',
      method: 'get',
    })
      .then(res => {
        const data = res.data.data;
        dispatch(changeList(data));
      })
      .catch(e => {
        console.log(e);
      });
  };
};
