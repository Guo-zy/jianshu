import * as constans from '../action/actionType';
import { fromJS } from 'immutable';

const defaluState = fromJS({
  focused: false,
  mouseIn: false,
  list: [],
  currentPage: 1,
  totalPage: 1,
});

const reducer = (state = defaluState, action) => {
  switch (action.type) {
    case constans.SERACH_FOCUSED:
      return state.set('focused', true);
    case constans.SEARCH_BLUR:
      return state.set('focused', false);
    case constans.CHANGE_LIST:
      // return state.set('list', action.data).set('totalPage', action.totalPage);
      return state.merge({
        list: action.data,
        totalPage: action.totalPage,
      });
    case constans.MOUSE_ENTER:
      return state.set('mouseIn', true);
    case constans.MOUSE_LEAVE:
      return state.set('mouseIn', false);
    case constans.CHANGE_PAGE:
      return state.set('currentPage', action.currentPage);
    default:
      return state;
  }
};

export default reducer;
