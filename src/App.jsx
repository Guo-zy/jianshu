import React, { Component, Fragment } from 'react';
import store from './store/index';
import Header from './common/header';
import Home from './pages/home';
import Detail from './pages/detail/loadable';
import Login from './pages/login';
import WriteArticle from './pages/write';
import { Globalstyle } from './style';
import { Provider } from 'react-redux';
import { BrowserRouter, Route } from 'react-router-dom';
class App extends Component {
  render() {
    return (
      <Fragment>
        <Globalstyle />
        <Provider store={store}>
          <BrowserRouter>
            <Header />
            <Route path="/" exact component={Home}></Route>
            <Route path="/login" exact component={Login}></Route>
            <Route path="/detail/:id" exact component={Detail}></Route>
            <Route path="/write" exact component={WriteArticle}></Route>
          </BrowserRouter>
        </Provider>
      </Fragment>
    );
  }
}

export default App;
